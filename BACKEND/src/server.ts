import express from 'express';
import path from 'path';
import 'express-async-errors';
import cors from 'cors';

import routes from './routes';

import './database/connection';
import errorHandler from './errors/handler';

const app = express();

app.use(cors());

app.use(express.json());

app.use(routes);
app.use('/uploads', express.static(path.join(__dirname, '..', 'uploads')));
app.use(errorHandler);
//Rota -> conjunto
//Recursos => usuario

//Método HTTP => GET, POST, PUT, DELETE
//Paramentros

//GET => Buscar uma informação
//POST => Criando uma informação
//PUT => atualizando uma informação
//DELETE => Deletando uma informação



app.listen(3333);